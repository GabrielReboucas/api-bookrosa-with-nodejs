'use strict';
function StateDAO(){

};
StateDAO.prototype.getState = (pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("SELECT id, name, uf, to_char(date_register, 'dd/MM/yyyy') as date_register FROM state", callback);
		done();
	});

};

StateDAO.prototype.getStateId = (stateid, pool, callback) =>{
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("SELECT id, name, uf, to_char(date_register, 'dd/MM/yyyy') as date_register FROM state WHERE id = $1",[stateid], callback);
		done();
	});
};

StateDAO.prototype.createState = (state, pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("INSERT INTO state (name, uf) VALUES($1, $2);",[state.name, state.uf], callback);
		done();
	});
};

StateDAO.prototype.updateState = (state, stateid, pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("UPDATE state SET name = $1, uf = $2 WHERE id = $3",[state.name, state.uf, stateid], callback);
		done();
	});

};

StateDAO.prototype.deleteState = (stateid, pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("DELETE FROM state WHERE id = $1 ",[stateid], callback);
		done();
	});


};


module.exports = () => {

	return StateDAO;
}