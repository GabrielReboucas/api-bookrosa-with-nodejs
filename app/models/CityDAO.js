'use strict'

function CityDAO(){

};

CityDAO.prototype.getCity = (pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("SELECT id, name, ibge, state_id, to_char(date_register, 'dd/MM/yyyy') as date_register FROM city", callback);
		done();
	});
};

CityDAO.prototype.getCityId = (cityid, pool, callback) =>{
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("SELECT id, name, ibge, state_id, to_char(date_register, 'dd/MM/yyyy') as date_register FROM city WHERE id = $1",[cityid], callback);
		done();
	});
};

CityDAO.prototype.createCity = (city, pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("INSERT INTO city (name, ibge, state_id) VALUES($1, $2, $3);",[city.name, city.ibge, city.state_id], callback);
		done();
	});
};

CityDAO.prototype.updateCity = (city, cityid, pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("UPDATE city SET name = $1, ibge = $2 , state_id = $3 WHERE id = $4",[city.name, city.ibge, city.state_id, cityid], callback);
		done();
	});
};

CityDAO.prototype.deleteCity = (cityid, pool, callback) => {
	pool.connect((err, client, done) => {
		if(err) {
			return console.error('error fetching client from pool', err);
		}
		client.query("DELETE FROM city WHERE id = $1",[cityid], callback);
		done();
	});
};


module.exports = () => {
	return CityDAO;
};