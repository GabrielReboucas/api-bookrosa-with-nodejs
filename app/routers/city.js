'use strict';

module.exports = (app) => {

	app.get('/api/city', (req, res) =>{
		let pool = app.config.postgresql();
		let CityDAO = new app.app.models.CityDAO;

		CityDAO.getCity(pool,(err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			res.json(result.rows);
		})
	});

	app.get('/api/city/:id', (req, res, next) => {
		let pool = app.config.postgresql();
		let CityDAO = new app.app.models.CityDAO;
		let cityid= req.params.id;

		CityDAO.getCityId(cityid, pool, (err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			res.json(result.rows);
		});
	});

	app.post('/api/city', (req, res) => {
		let city =req.body;
		let pool = app.config.postgresql();
		let CityDAO = new app.app.models.CityDAO;

		CityDAO.createCity(city, pool, (err, result)=>{
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}

			console.log('Cidade inserida com sucesso = ' + city)
			res.send(city);
		});
	});

	app.put('/api/city/:id', (req, res, next) => {
		let city=req.body;
		let cityid= req.params.id;
		let pool = app.config.postgresql();
		let CityDAO = new app.app.models.CityDAO;

		CityDAO.updateCity(city, cityid, pool, (err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			console.log('Cidade alterada com sucesso de id = ' + cityid)
			res.send(city);

		});
		
	});

	app.delete('/api/city/:id', (req, res, next) => {
		let cityid= req.params.id;
		let pool = app.config.postgresql();
		let CityDAO = new app.app.models.CityDAO;

		CityDAO.deleteCity(cityid, pool, (err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			console.log('Cidade excluida com sucesso de id = ' + cityid);
			res.send('Cidade excluida com sucesso de id = ' + cityid);

		});
	})
};