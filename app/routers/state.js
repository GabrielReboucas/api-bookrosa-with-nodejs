'use strict';

module.exports = (app) => {
	app.get('/api/state', (req, res) =>{
		let pool = app.config.postgresql();
		let StateDAO = new app.app.models.StateDAO;

		StateDAO.getState(pool,(err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			res.json(result.rows);
		})
	});

	app.get('/api/state/:id', (req, res, next) => {
		let pool = app.config.postgresql();
		let StateDAO = new app.app.models.StateDAO;
		let stateid= req.params.id;

		StateDAO.getStateId(stateid, pool, (err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			res.json(result.rows);
		});
	});

	app.post('/api/state', (req, res) => {
		let state=req.body;
		let pool = app.config.postgresql();
		let StateDAO = new app.app.models.StateDAO;

		StateDAO.createState(state, pool, (err, result)=>{
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}

			console.log('Estado inserido com sucesso = ' + state)
			res.send(state);
		});
	});

	app.put('/api/state/:id', (req, res, next) => {
		let state=req.body;
		let stateid= req.params.id;
		let pool = app.config.postgresql();
		let StateDAO = new app.app.models.StateDAO;

		StateDAO.updateState(state, stateid, pool, (err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			console.log('Estado Alterado com sucesso = ' + state)
			res.send(state);

		});
		
	});

	app.delete('/api/state/:id', (req, res, next) => {
		let stateid= req.params.id;
		let pool = app.config.postgresql();
		let StateDAO = new app.app.models.StateDAO;

		StateDAO.deleteState(stateid, pool, (err, result) => {
			if(err) {
				res.send(err);
				return console.error('error running query', err);
			}
			res.send('Estado excluido com sucesso de id = ' + stateid);

		});
	})

};