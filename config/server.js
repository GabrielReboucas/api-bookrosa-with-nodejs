'use strict'

let express = require('express');
let app = express();
let path = require('path');
let consign = require('consign');
let bodyParser = require('body-parser');
let jsonParser = bodyParser.json();

app.use('/static', express.static(path.join(__dirname, '../public')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

consign()
.include('./config/postgresql.js')
.then('./app/models/')
.then('./app/routers/')
.into(app);

module.exports = app;