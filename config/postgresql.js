'use strict'

let pg = require('pg');

//var connPg = () => {
	//console.log('Conexão feita com o banco de dados');
	//return "postgres://postgres:root@localhost/bookrosa";
//};
//module.exports = () => {
	//console.log('AutoLoad do Banco de Dados carregado')
	//return connPg;

//}

let config = {
  user: 'postgres', //env var: PGUSER
  database: 'bookrosa', //env var: PGDATABASE
  password: 'root', //env var: PGPASSWORD
  host: 'localhost', // Server hosting the postgres database
  port: 5432, //env var: PGPORT
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

let createConnection = () => {
	console.log("Conexão feita com o banco de dados");
	return new pg.Pool(config);
}

module.exports = () => {
	console.log('AutoLoad do Banco de Dados carregado')
	return createConnection;
}