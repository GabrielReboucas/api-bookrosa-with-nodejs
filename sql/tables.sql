CREATE TABLE state
(
	id SERIAL NOT NULL,
	name VARCHAR(100) NOT NULL,
	uf VARCHAR(2) NOT NULL,
	date_register timestamp(0) without time zone NOT NULL DEFAULT now(),
	CONSTRAINT pk_stateid PRIMARY KEY (id),
);

CREATE TABLE city 
(
	id SERIAL NOT NULL,
	name VARCHAR(50) NOT NULL,
	ibge VARCHAR(8) NOT NULL,
	state_id integer NOT NULL,
	date_register timestamp(0) without time zone NOT NULL DEFAULT now(),
	CONSTRAINT pk_cityid PRIMARY KEY (id),
	CONSTRAINT fk_statecity FOREIGN KEY (state_id) REFERENCES state (id) 
);

CREATE TABLE user_client
(
	id SERIAL NOT NULL,
	full_name VARCHAR(100) NOT NULL,
	date_birth DATE NOT NULL,
	phone VARCHAR(11) NOT NULL,
	phone2 VARCHAR(11),
	state_id integer NOT NULL,
	city_id integer NOT NULL,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(32) NOT NULL,
	date_register timestamp(0) without time zone NOT NULL DEFAULT now(),
	CONSTRAINT pk_userid PRIMARY KEY (id),
	CONSTRAINT fk_stateuser_client FOREIGN KEY (state_id) REFERENCES state (id),
	CONSTRAINT fk_cityuser_client FOREIGN KEY (state_id) REFERENCES city (id),
	CONSTRAINT un_useremailpswd UNIQUE (email, password)
);